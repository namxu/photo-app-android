package com.namxu.multipicturecamera.base.prefrencecustom

import java.lang.reflect.Type

interface JsonConverter {
    fun <T> fromJson(json: String, typeOfT: Type): T

    fun <T> toJson(`object`: T, typeOfT: Type): String
}
