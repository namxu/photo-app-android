@file:Suppress("unused")

package com.namxu.multipicturecamera.base

import android.annotation.SuppressLint
import android.content.Context
import com.google.gson.Gson
import com.namxu.multipicturecamera.base.prefrencecustom.PhotoMultiPreference
import com.namxu.multipicturecamera.base.prefrencecustom.TypeToken
import java.io.Serializable

@SuppressLint("SyntheticAccessor")
class DataManager {

    companion object {
        private const val MY_PREFS_NAME = "PHOTO_MULTI_PICKER"
        private val savePreference =
            PhotoMultiPreference(
                BaseApp.context.getSharedPreferences(
                    MY_PREFS_NAME,
                    Context.MODE_PRIVATE
                )
            )
//        var sharedInstantUser: User
//            get() = getSavedUser()
//            set(value) = savedUser(value)
//
//        private fun getSavedUser(): User {
//            return savePreference["user", User::class.java, User()]!!
//        }

        public fun getDeviceHeight(): Int {
            return (savePreference["height_window", String::class.java, "0"] ?: "0").toInt()
        }

        public fun setDeviceHeight(height: Int) {
            return savePreference.put("height_window", height.toString())
        }

        public fun getDeviceWidth(): Int {
            return (savePreference["width_window", String::class.java, "0"] ?: "0").toInt()
        }

        public fun setDeviceWidth(height: Int) {
            return savePreference.put("width_window", height.toString())
        }

//        private fun getSavedUserFormDB(): User {
//            val user = User()
//            return user.getSavedUser(sharedInstantUser.id!!)
//        }
//
//        private fun savedUser(user: User) {
//            savePreference.put("user", user)
//        }

        fun clearAll() {
            savePreference.remove("isLogin")
            savePreference.clear()
        }



        fun <T> getStringFromList(item: ArrayList<T>): String {
            val gSon = Gson()
            val listString = gSon.toJsonTree(item)
            return listString.toString()
        }

        fun <T> getListFromString(item: String): ArrayList<T> {
            val gSon = Gson()
            val type = object : TypeToken<ArrayList<T>>() {
            }.type
            return if (item.isNotEmpty()) {
                gSon.fromJson(item, type) as ArrayList<T>
            } else {
                ArrayList()
            }
        }


        fun saveDashboardPhotoAlbum(item: ArrayList<PhotoAlbum>) {
            val gSon = Gson()
            val listString = gSon.toJsonTree(item)
            savePreference.put("photos_album_list", listString)
        }


        fun getDashboardPhotoAlbum(): ArrayList<PhotoAlbum> {
            val gSon = Gson()
            val jsonCars = savePreference["photos_album_list", String::class.java, ""] ?: ""
            val type = object : TypeToken<ArrayList<PhotoAlbum>>() {
            }.type
            return if (jsonCars.isNotEmpty()) {
                gSon.fromJson(jsonCars, type) as ArrayList<PhotoAlbum>
            } else {
                ArrayList()
            }
        }
    }


}
class PhotoAlbum : Serializable {
    var filePath = ""
    var name = ""
    var albumId = 0

    constructor(filePath: String) {
        this.filePath = filePath
    }
}
