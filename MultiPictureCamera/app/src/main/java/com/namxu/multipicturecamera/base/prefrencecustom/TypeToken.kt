package com.namxu.multipicturecamera.base.prefrencecustom

import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

// Inspired by Gson's TypeToken
abstract class TypeToken<T> {
    val type: Type

    constructor() {
        // Superclass of anonymous class retains its type parameter despite of type erasure.
        val superclass = javaClass.genericSuperclass
        if (superclass is Class<*>) {
            throw RuntimeException("Missing type parameter.")
        }
        val parameterizedSuperclass = superclass as ParameterizedType
        type = parameterizedSuperclass.actualTypeArguments[0]
    }

    private constructor(classOfT: Class<*>?) {
        if (classOfT == null) {
            throw NullPointerException("classOfT == null")
        }
        this.type = classOfT
    }

    companion object {

        internal fun <T> fromClass(classForT: Class<T>): TypeToken<T> {
            return object : TypeToken<T>(classForT) {

            }
        }

        internal fun <T : Any> fromValue(value: T): TypeToken<T> {
            return object : TypeToken<T>(value.javaClass) {

            }
        }
    }
}
