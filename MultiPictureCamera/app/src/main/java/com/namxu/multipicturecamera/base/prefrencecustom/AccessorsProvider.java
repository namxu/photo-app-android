package com.namxu.multipicturecamera.base.prefrencecustom;

import java.util.Map;

interface AccessorsProvider {
    Map<Class<?>, Accessor<?>> getAccessors();
}
