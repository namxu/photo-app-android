package com.namxu.multipicturecamera.base.prefrencecustom;

class Preconditions {

    private Preconditions() {
    }

    protected static Preconditions create() {
        return new Preconditions();
    }

    static void checkNotNull(final Object object, final String message) {
        if (object == null) {
            throw new NullPointerException(message);
        }
    }
}
