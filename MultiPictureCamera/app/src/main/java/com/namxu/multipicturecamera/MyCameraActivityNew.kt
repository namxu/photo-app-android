package com.namxu.multipicturecamera

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.*
import android.view.View
import android.webkit.MimeTypeMap
import android.widget.Toast
import com.namxu.multipicturecamera.base.*
import com.otaliastudios.cameraview.*
import com.otaliastudios.cameraview.CameraView
import id.zelory.compressor.Compressor
import kotlinx.android.synthetic.main.activity_my_camera.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class MyCameraActivityNew : BaseActivity() {
    lateinit var cameraView: CameraView
    var isFront: Boolean = false
    var isCapturing: Boolean = false
    internal lateinit var file: File
    internal var countDownTimer: CountDownTimer? = null
    private var startTime = 0L
    private val customHandler = Handler()
    internal var timeInMilliseconds = 0L
    internal var timeSwapBuff = 0L
    internal var updatedTime = 0L
    private var mCaptureTime: Long = 0
    lateinit var imageFile: File
    private val FLIP_VERTICAL = 1
    private val FLIP_HORIZONTAL = 2
    private var imagePath: String = ""
    private var pickerFrom: String = ""
    //    private var cameraType = "";
    var finalImage: Bitmap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        makeFullScreen()
        setContentView(R.layout.activity_my_camera)

        if (intent?.extras != null) {
            if (intent?.getStringExtra(Constants.VIDEO).equals(Constants.VIDEO)) {
                video_capture.visibility = View.VISIBLE
                image_capture_.visibility = View.GONE
                text_time.visibility = View.GONE
                pickerFrom = Constants.VIDEO
            } else if (intent?.getStringExtra(Constants.IMAGE).equals(Constants.IMAGE)) {
                image_capture_.visibility = View.VISIBLE
                image_gallery.visibility = View.GONE
                video_capture.visibility = View.GONE
                text_time.visibility = View.GONE
                pickerFrom = Constants.IMAGE
            } else if (intent?.getStringExtra(Constants.BOTH).equals(Constants.BOTH)) {
                image_capture_.visibility = View.VISIBLE
                video_capture.visibility = View.VISIBLE
                text_time.visibility = View.GONE
                pickerFrom = Constants.BOTH
            } else if (intent?.getStringExtra(Constants.NONE).equals(Constants.NONE)) {
                image_capture_.visibility = View.VISIBLE
                image_gallery.visibility = View.GONE
                video_capture.visibility = View.GONE
                text_time.visibility = View.GONE
                pickerFrom = Constants.NONE
            }
        }
        cameraView = findViewById(R.id.camera_view)
        cameraView.mode = Mode.PICTURE
        if (intent?.getBooleanExtra("selfie", false)!!) {
            cameraView.facing = Facing.FRONT
            image_toggle_camera.visibility = View.GONE
        }

        image_close.setOnClickListener {
            this.setResult(0)

            finish()
        }

        image_toggle_camera.setOnClickListener { switchCamera() }

        video_capture.setOnClickListener {
            captureVideo()

        }

        cameraView.addCameraListener(object : CameraListener() {
            override fun onPictureTaken(result: PictureResult) {
                super.onPictureTaken(result)

                AsyncTask.THREAD_POOL_EXECUTOR.execute {
                    createFile("NewFile") {outerFile ->

//                        result.toFile(outerFile) { file ->
//                            val list = DataManager.getDashboardPhotoAlbum()
//                            list.add(0, PhotoAlbum(file?.absolutePath ?: outerFile.absolutePath))
//                            DataManager.saveDashboardPhotoAlbum(list)
//                        }
                        try {
                            val outputStream =
                                FileOutputStream(outerFile.path)
                            outputStream.write(result.data)
                            outputStream.close()
                            val list = DataManager.getDashboardPhotoAlbum()
                            list.add(0, PhotoAlbum(outerFile.absolutePath))
                            DataManager.saveDashboardPhotoAlbum(list)
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                }
//                result.toFile(imageFile) { file ->
//                    val compressFile =
//                        Compressor(this@MyCameraActivityNew).setQuality(60).compressToFile(file!!)
//                    setResult(
//                        RESULT_OK,
//                        Intent().putExtra("FILE-PATH", compressFile!!.absoluteFile.toString())
//                    )
//                    finish()
//                }
            }

            override fun onVideoTaken(result: VideoResult) {
                super.onVideoTaken(result)
                setResult(
                    Activity.RESULT_OK,
                    Intent().putExtra("FILE-PATH", result.file.absolutePath)
                )
                finish()
            }
        })

        image_capture_.setOnClickListener {
            cameraView.mode = Mode.PICTURE
            Handler().postDelayed({
                capturePicture()
            }, 200)
        }

        image_gallery.setOnClickListener {
            //            Dexter.withActivity(this@MyCameraActivityNew)
//                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                .withListener(object : PermissionListener {
//                    override fun onPermissionGranted(response: PermissionGrantedResponse) {
//                        if (pickerFrom.equals(Constants.IMAGE)) {
//                            val galleryIntent = Intent(Intent.ACTION_PICK,
//                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
//                            startActivityForResult(galleryIntent,
//                                Constants.IMAGE_VIDEO_REQUEST_CODE)
//                        }
//                        else if (pickerFrom.equals(Constants.VIDEO)) {
//                            val galleryIntent = Intent(Intent.ACTION_PICK,
//                                MediaStore.Video.Media.EXTERNAL_CONTENT_URI)
//                            startActivityForResult(galleryIntent,
//                                Constants.IMAGE_VIDEO_REQUEST_CODE)
//                        }
//                        else {
//                            val galleryIntent = Intent(Intent.ACTION_OPEN_DOCUMENT,
//                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
//                            galleryIntent.addCategory(Intent.CATEGORY_OPENABLE)
//                            galleryIntent.type = "image/* video/*"
//                            galleryIntent.putExtra(Intent.EXTRA_MIME_TYPES,
//                                arrayOf("image/*", "video/*"))
//                            startActivityForResult(Intent.createChooser(galleryIntent,
//                                "Select Picture"), Constants.IMAGE_VIDEO_REQUEST_CODE)
//                        }
//                    }
//
//                    override fun onPermissionDenied(response: PermissionDeniedResponse) {
//                        // check for permanent denial of permission
//                        if (response.isPermanentlyDenied) {
//                            Toast.makeText(applicationContext,
//                                "Storage Permission Denied",
//                                Toast.LENGTH_SHORT).show()
//                        }
//                    }
//
//                    override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest,
//                                                                    token: PermissionToken) {
//                        token.continuePermissionRequest()
//                    }
//                }).check()
        }
        container_flash.setOnClickListener { toggleFlash() }
    }

    private fun capturePicture() {
        if (cameraView.isTakingPicture) return
        mCaptureTime = System.currentTimeMillis()
        cameraView.takePicture()

    }


    fun toggleCamera(view: View) {
        cameraView.toggleFacing()
        isFront = !isFront

    }

    fun resetTimer() {
        try {

            timeSwapBuff += timeInMilliseconds
            customHandler.removeCallbacks(updateTimerThread)
            startTime = 0L
            updatedTime = 0L
            timeSwapBuff = 0L
            timeInMilliseconds = 0L
            text_time.visibility = View.INVISIBLE
            if (cameraView.isTakingVideo) {

                cameraView.stopVideo()
            }

        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun switchCamera() {
        cameraView.toggleFacing()
        isFront = !isFront
        if(isFront){
            image_toggle_camera.setImageResource(R.drawable.ic_facing_front)
        }else{
            image_toggle_camera.setImageResource(R.drawable.ic_facing_back)
        }
    }

    private fun toggleFlash() {
        when(cameraView.flash){
            Flash.AUTO -> {

                is_flashed.visibility = View.VISIBLE
                ivFlashImage.setImageResource(R.drawable.ic_flash_torch)
                cameraView.flash = Flash.TORCH
            }
            Flash.OFF -> {
                is_flashed.visibility = View.GONE
                ivFlashImage.setImageResource(R.drawable.ic_flash_on)
                cameraView.flash = Flash.ON

            }
            Flash.ON -> {
                is_flashed.visibility = View.GONE
                ivFlashImage.setImageResource(R.drawable.ic_flash_auto)
                cameraView.flash = Flash.AUTO
            }
            Flash.TORCH -> {
                is_flashed.visibility = View.GONE
                cameraView.flash = Flash.OFF
                ivFlashImage.setImageResource(R.drawable.ic_flash_off)
            }
        }
    }


    private fun captureVideo() {
        if (cameraView.isTakingVideo) {
            cameraView.stopVideo()
        } else {
            cameraView.mode = Mode.VIDEO
            file = saveVideo()!!
            cameraView.takeVideo(file, 30000)
            startTime = SystemClock.uptimeMillis()
            customHandler.postDelayed(updateTimerThread, 0)
        }
    }




    private val updateTimerThread = object : Runnable {

        override fun run() {

            timeInMilliseconds = SystemClock.uptimeMillis() - startTime

            updatedTime = timeSwapBuff + timeInMilliseconds

            var secs = (updatedTime / 1000).toInt()
            val mins = secs / 60
            secs = secs % 60
            val milliseconds = (updatedTime % 1000).toInt()
            text_time.text = ("" + mins + ":" + String.format("%02d", secs))
            if (text_time.text.toString() == "0:31") {
//                vibrate()
                resetTimer()
            }
            customHandler.postDelayed(this, 0)
        }

    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.IMAGE_VIDEO_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val path = data?.data
            val imageFilePath = getPathForVideoImage(applicationContext, path!!)
            val mimeType = getMimeType(imageFilePath)
            if (mimeType?.contains("video") == true) {
                setResult(RESULT_OK, Intent().putExtra("FILE-PATH", imageFilePath))
                finish()
            } else {
                val f = File(imageFilePath)
                val tempFile = File.createTempFile("img", "temp")
                tempFile.delete()
                f.copyTo(tempFile)
                val compressFile =
                    Compressor(this@MyCameraActivityNew).setQuality(60).compressToFile(tempFile)

                setResult(
                    RESULT_OK,
                    Intent().putExtra("FILE-PATH", compressFile.absoluteFile.toString())
                )
                finish()
            }

        }
    }

    private fun getMimeType(fileUrl: String): String? {
        val extension = MimeTypeMap.getFileExtensionFromUrl(fileUrl);
        return MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
    }




    private fun getPathForVideoImage(context: Context, uri: Uri): String {
        return AdvanceFileUtils.getRealPath(context, uri)
    }

    private fun saveVideo(): File? {

        val root = Environment.getExternalStorageDirectory().toString()
        val myDir = File("$root/saved_videos")
        myDir.mkdirs()
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        val fName = "CONTENT_BUDDY_$timeStamp.mp4"
        val file = File(myDir, fName)
        if (file.exists()) file.delete()
        if (file.parentFile?.exists() == false) file.parentFile?.mkdirs()
        file.createNewFile()
        try {
            val out = FileOutputStream(file)
            out.flush()
            out.close()
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(applicationContext, "Give Permission", Toast.LENGTH_SHORT).show()
            return null
        }
        return file
    }

    override fun onResume() {
        super.onResume()
        cameraView.open()
    }

    override fun onPause() {
        super.onPause()
        cameraView.close()
    }

    override fun onDestroy() {
        super.onDestroy()
        cameraView.destroy()
    }

}