package com.namxu.multipicturecamera.base.prefrencecustom;

interface Accessor<T> {
    T get(String key, T defaultValue);

    void put(String key, T value);
}
