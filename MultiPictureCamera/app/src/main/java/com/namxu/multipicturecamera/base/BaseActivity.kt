package com.namxu.multipicturecamera.base

import android.R.id
import android.app.ProgressDialog
import android.content.Context
import android.os.Environment
import android.view.View
import android.view.Window
import android.view.WindowManager.LayoutParams
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import java.io.File
import java.io.IOException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.roundToInt

abstract class BaseActivity : AppCompatActivity() {


    val READ_WRITE_STORAGE = 52
    private var mProgressDialog: ProgressDialog? = null



    open fun makeFullScreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            LayoutParams.FLAG_FULLSCREEN,
            LayoutParams.FLAG_FULLSCREEN
        )
    }

    protected open fun showLoading(message: String) {
        mProgressDialog = ProgressDialog(this)
        mProgressDialog?.setMessage(message)
        mProgressDialog?.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        mProgressDialog?.setCancelable(false)
        mProgressDialog?.show()
    }

    protected open fun hideLoading() {
        mProgressDialog?.dismiss()
    }

    protected open fun showSnackbar(message: String) {
        val view = findViewById<View?>(id.content)
        if (view != null) {
            Snackbar.make(
                view,
                message,
                Snackbar.LENGTH_SHORT
            ).show()
        } else {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        }
    }


    private fun getPrivateAlbumStorageDir(
        context: Context,
        albumName: String
    ): File? {
        // Get the directory for the app's private pictures directory.
//        val file = File(
//            context.getExternalFilesDir(
//                Environment.DIRECTORY_MOVIES
//            ), albumName
//        )
        val file = File(
            checkPathForSave(context)
            , albumName
        )
        if (!file.mkdirs()) {
            file.mkdirs()
        }
        return file
    }
    fun formatSize(sizeS: Long): String {
        var size = sizeS
        var suffix = ""
        if (size >= 1024L) {
            suffix = "KB"
            size /= 1024L
            if (size >= 1024L) {
                suffix = "MB"
                size /= 1024L
            }
        }
        val resultBuffer = StringBuilder(size.toInt())

        var commaOffset = resultBuffer.length - 3
        while (commaOffset > 0) {
            resultBuffer.insert(commaOffset, ',')
            commaOffset -= 3
        }
        resultBuffer.append(suffix)
        return resultBuffer.toString()
    }

    private fun checkPathForSave(context: Context): String {
        val freeBytesInternal = File(context.filesDir.absoluteFile.toString()).freeSpace
        val isSDPresent =
            Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED
        return "${context.getExternalFilesDir(Environment.DIRECTORY_MOVIES)}" + "/.DocSkan"
//        if (isSDPresent) {
//            val list = context.getExternalFilesDirs(Environment.DIRECTORY_MOVIES)
//            var freeBytesExternal =
//                File(context.getExternalFilesDir(Environment.DIRECTORY_MOVIES).toString()).freeSpace
//            var freeBytesSD = 0L
//            if (list.size > 1) {
//                freeBytesExternal =
//                    File(list.first().toString()).freeSpace
//                freeBytesSD = File(list.last().toString()).freeSpace
//            }
//            return if (freeBytesInternal > freeBytesExternal) {
//                if (freeBytesInternal > freeBytesSD) {
//                    "${context.filesDir}/.DocSkan"
//                } else {
//                    "${list.last()}" + "/.DocSkan"
//                }
//            } else if (freeBytesExternal > freeBytesSD) {
//                "${context.getExternalFilesDir(Environment.DIRECTORY_MOVIES)}" + "/.DocSkan"
//            } else {
//                "${list.last()}" + "/.DocSkan"
//            }
//        } else {
//            return "${context.filesDir}/.DocSkan"
//        }

    }


    fun getDirectoryPath(context: BaseActivity): String {
        return getPrivateAlbumStorageDir(context, "/.SKansVideos/")?.absolutePath + "/"
    }

    private fun createNoMediaFile() {
        val noMedia = getDirectoryPath(this@BaseActivity) + ".nomedia"
        val newFile = File(noMedia)
        try {
            if (newFile.parentFile?.exists() == false)
                newFile.parentFile?.mkdirs()
            if (!newFile.exists())
                newFile.createNewFile()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    fun getNameForFile(dir: String, name: String): String {
        val file = dir + name + System.nanoTime() + ".mp4"
        val newFile = File(file)
        try {
            if (!newFile.parentFile!!.exists())
                newFile.parentFile?.mkdirs()
            if (!newFile.exists())
                newFile.createNewFile()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return dir + name + DateConverter.currentDate.replace(
            "/",
            ":"
        ) + DateConverter.currentTime + System.nanoTime() + ".mp4"
    }

    @Throws(IOException::class)
    fun createPdfFile(name: String, callBack: (File) -> Unit) {
        val dir = getPrivateAlbumStorageDir(this, "/.SKansVideos/")?.absolutePath + "/"
//        val file = "$dir$name.pdf"
        val file = "$dir$name"
        val newFile = File(file)
        try {
            if (!newFile.parentFile!!.exists())
                newFile.parentFile?.mkdirs()
            if (!newFile.exists())
                newFile.createNewFile()
            callBack(newFile)
        } catch (e: IOException) {
            e.printStackTrace()
            throw IOException(e)
        }
        createNoMediaFile()
    }

    @Throws(IOException::class)
    fun createFile(name: String, callBack: (File) -> Unit) {
        val dir = getPrivateAlbumStorageDir(this, "/.FilesMultiPhoto/")?.absolutePath + "/"
        val file =
            dir + name + if (name == "NewFile") DateConverter.currentTime + System.nanoTime() + ".jpeg" else ""
        val newFile = File(file)
        try {
            if (!newFile.parentFile!!.exists())
                newFile.parentFile?.mkdirs()
            if (!newFile.exists())
                newFile.createNewFile()
            callBack(newFile)
        } catch (e: IOException) {
            e.printStackTrace()
            throw IOException(e)
        }
        createNoMediaFile()
    }

    @Throws(IOException::class)
    fun createFileReturn(name: String):File {
        val dir = getPrivateAlbumStorageDir(this, "/.FilesMultiPhoto/")?.absolutePath + "/"
        val file =
            dir + name + if (name == "NewFile") DateConverter.currentTime + System.nanoTime() + ".jpeg" else ""
        val newFile = File(file)
        try {
            if (!newFile.parentFile!!.exists())
                newFile.parentFile?.mkdirs()
            if (!newFile.exists())
                newFile.createNewFile()
            createNoMediaFile()
            return newFile
        } catch (e: IOException) {
            e.printStackTrace()
            throw IOException(e)
        }

    }

    fun createFileWithOutExisting(name: String, callBack: (File) -> Unit) {
        val dir = getPrivateAlbumStorageDir(this, "/.SKansVideos/")?.absolutePath + "/"
        val file =
            dir + name + if (name == "NewFile") DateConverter.currentTime + System.nanoTime() + ".mp4" else ""
        val newFile = File(file)
        try {
            if (!newFile.parentFile!!.exists())
                newFile.parentFile?.mkdirs()
            if (newFile.exists()) {
                newFile.delete()
            }
            callBack(newFile)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        createNoMediaFile()
    }

}


fun String.capitalized(): String {
    return this.substring(0, 1).toUpperCase(Locale.getDefault()) + this.substring(1)
}

fun <T> generateList(response: String, type: Class<Array<T>>): ArrayList<T> {
    val arrayList = ArrayList<T>()
    if(response.isEmpty() || response.equals("null")|| response.equals("\"[]\"")){
        return arrayList
    }
    arrayList.addAll(listOf(*Gson().fromJson<Array<T>>(response, type)))
    return arrayList
}

fun Any.toNotNullString(): String {
    //    Log.d("toString",this.toString())
    return if (this == "null") {
        this.toString().replace("null", "")
    }
    else {
        this.toString()
    }
}

fun <T> List<T>.toArrayList(): ArrayList<T> {
    return ArrayList(this)
}


fun View.viewVisible() {
    this.visibility = View.VISIBLE
}

fun View.viewGone() {
    this.visibility = View.GONE
}

fun View.viewInVisible() {
    this.visibility = View.INVISIBLE
}

fun View.viewVisibility(): Int {
    return this.visibility
}

fun View.isVisible(): Boolean {
    return this.visibility == View.VISIBLE
}

fun View.isGone(): Boolean {
    return this.visibility == View.GONE
}

fun View.isInVisible(): Boolean {
    return this.visibility == View.INVISIBLE
}


@Suppress("DEPRECATION", "unused")
object DateConverter {


    val currentDate: String
        get() {
            val calendar = Calendar.getInstance()
            val date = calendar.time

            val timeFormat = SimpleDateFormat("MM/dd/yyyy", Locale.getDefault())
            return timeFormat.format(date)

        }
    val currentDateFile: String
        get() {
            val calendar = Calendar.getInstance()
            val date = calendar.time

            val timeFormat = SimpleDateFormat("MM:dd:yyyy", Locale.getDefault())
            return timeFormat.format(date)

        }
    val currentTime: String
        get() {
            val calendar = Calendar.getInstance()
            val date = calendar.time
            val timeFormat = SimpleDateFormat("MMddyyyy_hh:mm:ss", Locale.getDefault())
            return timeFormat.format(date)

        }


    fun convertDate(date_string: String): String {
        var dateString = date_string
        dateString = utcToLocal(dateString)
        val dateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault())
        var myDate = Date()
        try {
            myDate = dateFormat.parse(dateString) ?: Date()

        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val timeFormat = SimpleDateFormat("MM.dd.yyyy hh:mm aa", Locale.getDefault())
        return timeFormat.format(myDate)
    }

    fun convertDate(date_string: Date, formatString: String = "dd-MM-yyyy hh:mm a"): String {
        val timeFormat = SimpleDateFormat(formatString, Locale.getDefault())
        return timeFormat.format(date_string)
    }

    fun convertLongToTime(time: Long,formatDate: String = "yyyy-MM-dd hh:mm:ss"): String {
        val date = Date(time)
        val format = SimpleDateFormat(formatDate, Locale.getDefault())
        return format.format(date)
    }

    fun convertTimeToDate(time_string: String): Date {
        val dateFormat = SimpleDateFormat("hh:mm:ss", Locale.getDefault())
        var myDate: Date? = null
        try {
            myDate = dateFormat.parse(time_string)

        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return myDate!!
    }

    fun convertStringToDate(time_string: String): Date {
        val dateFormat = SimpleDateFormat("MM:dd:yyyy", Locale.getDefault())
        return try {
            dateFormat.parse(time_string) ?: Date()
        } catch (e: ParseException) {
            Date()
        }
    }

    fun convertDateTrial(date_string: String): String {
        var dateString = date_string
        dateString = utcToLocal(dateString)
        val dateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault())
        var myDate: Date? = null
        try {
            myDate = dateFormat.parse(dateString)

        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val timeFormat = SimpleDateFormat("MM.dd.yyyy", Locale.getDefault())
        return "End At: " + timeFormat.format(myDate ?: Date())
    }

    fun convertDateReward(date_string: String): String {
        var dateString = date_string
        dateString = utcToLocal(dateString)
        val dateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault())
        var myDate: Date? = null
        try {
            myDate = dateFormat.parse(dateString)

        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val timeFormat = SimpleDateFormat("MM-dd-yyyy", Locale.getDefault())
        return timeFormat.format(myDate ?: Date())
    }

    fun convertDateWithMonthName(date_string: String): String {
        var dateString = date_string
        dateString = utcToLocal(dateString)
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        var myDate: Date? = null
        try {
            myDate = dateFormat.parse(dateString)

        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val timeFormat = SimpleDateFormat("MMM. dd, yyyy, hh:mm aa", Locale.getDefault())
        return timeFormat.format(myDate ?: Date())
    }


    fun convertTime(date_string: String): String {
        val dateFormat = SimpleDateFormat("HH:mm:ss", Locale.getDefault())
        var myDate: Date? = null
        try {
            myDate = dateFormat.parse(date_string)

        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val timeFormat = SimpleDateFormat("hh:mm", Locale.getDefault())
        return timeFormat.format(myDate ?: Date())
    }

    fun convertTimeHour(date_string: String): String {
        val dateFormat = SimpleDateFormat("HH:mm:ss", Locale.getDefault())
        var myDate: Date? = null
        try {
            myDate = dateFormat.parse(date_string)

        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val timeFormat = SimpleDateFormat("hh:mm a", Locale.getDefault())
        return timeFormat.format(myDate ?: Date())
    }

    fun convertDateWithCompleteDetails(date_string: String): String {
        var dateString = date_string
        dateString = utcToLocal(dateString)
        val dateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault())
        var myDate: Date? = null
        try {
            myDate = dateFormat.parse(dateString)

        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val timeFormat = SimpleDateFormat("EEEE,dd,MMMM,yyyy, hh:mm aa", Locale.getDefault())
        return timeFormat.format(myDate ?: Date())
    }


    fun getDateAsAdiFormat(date_string: String): String? {
        val dateFormat = SimpleDateFormat("EEEE dd-MMMM, yyyy HH:mm:ss", Locale.getDefault())
        var myDate: Date? = null
        try {
            myDate = dateFormat.parse(date_string)

        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val timeFormat = SimpleDateFormat("yyyy-MM-dd  HH:mm:ss", Locale.getDefault())
        return localToUtc(timeFormat.format(myDate ?: Date()))
    }

    fun getCustomDateString(date: String, format: String): String {
        val dateFormat = SimpleDateFormat(format, Locale.getDefault())
        val dateItem: Date? = dateFormat.parse(date)
        val tmp = SimpleDateFormat("MMMM d", Locale.getDefault())
        var str = tmp.format(dateItem ?: Date())
        str = str.substring(0, 1).toUpperCase(Locale.getDefault()) + str.substring(1)
        str += if (dateItem!!.date > 10 && dateItem.date < 14)
            "th"
        else {
            when {
                str.endsWith("1") -> "st"
                str.endsWith("2") -> "nd"
                str.endsWith("3") -> "rd"
                else -> "th"
            }
        }
        return str
    }


    private fun localToUtc(s: String): String? {
        try {
            val utcFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
            utcFormat.timeZone = TimeZone.getDefault()
            val date: Date?
            date = utcFormat.parse(s)
            val localTimeFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
            localTimeFormat.timeZone = TimeZone.getTimeZone("UTC")
            return localTimeFormat.format(date ?: Date())
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return null
    }

    fun tierNew(s: String): String? {
        val input = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault())
        val output = SimpleDateFormat("MMM dd, yyyy", Locale.getDefault())

        var d: Date? = null
        try {
            d = input.parse(s)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return output.format(d ?: Date())

    }

    private fun utcToLocal(s: String): String {
        try {
            val utcFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
            utcFormat.timeZone = TimeZone.getTimeZone("UTC")
            val date: Date?
            date = utcFormat.parse(s)
            val localTimeFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
            localTimeFormat.timeZone = TimeZone.getDefault()
            return localTimeFormat.format(date ?: Date())
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return Date().toString()
    }


    fun convertSpecialWallTop(date_string: String): String {
        var dateString = date_string
        dateString = utcToLocal(dateString)
        val dateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault())
        val dateFormatHalf = SimpleDateFormat("hh:mm aa", Locale.getDefault())
        val myDate: Date?
        try {
            myDate = dateFormat.parse(dateString)

        } catch (e: ParseException) {
            e.printStackTrace()
            return ""
        }

        val timeFormat = SimpleDateFormat("MMM dd 'at' hh:mm aa", Locale.getDefault())
        val calenderDate = Calendar.getInstance()
        calenderDate.time = myDate ?: Date()
        val today = Calendar.getInstance()
        val yesterday = Calendar.getInstance()
        yesterday.add(Calendar.DATE, -1)


        return if (calenderDate.get(Calendar.YEAR) == today.get(Calendar.YEAR) && calenderDate.get(
                Calendar.DAY_OF_YEAR
            ) == today.get(
                Calendar.DAY_OF_YEAR
            )
        ) {
            "Today at " + dateFormatHalf.format(myDate ?: Date())
        } else if (calenderDate.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR) && calenderDate.get(
                Calendar.DAY_OF_YEAR
            ) == yesterday.get(
                Calendar.DAY_OF_YEAR
            )
        ) {
            "Yesterday at " + dateFormatHalf.format(myDate ?: Date())
        } else {
            timeFormat.format(myDate ?: Date())
        }

    }

    private const val ALLOWED_CHARACTERS = "123456789qwertyuiopasdfghjklzxcvbnm-_)("
    fun getRandomString(sizeOfRandomString: Int): String {
        val random = Random()
        val sb = StringBuilder(sizeOfRandomString)
        for (i in 0 until sizeOfRandomString)
            sb.append(ALLOWED_CHARACTERS[random.nextInt(ALLOWED_CHARACTERS.length)])
        return sb.toString()
    }

    private fun randBetween(start: Int, end: Int): Int {
        return start + (Math.random() * (end - start)).roundToInt()
    }

    fun getRandumNumber(): Int {
        return randBetween(0, 99)
    }

    fun randomDate(): String {
        val dfDateTime = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault())
        val year = randBetween(2005, 2018)// Here you can set Range of years you need
        val month = randBetween(0, 11)
        val hour = randBetween(9, 22) //Hours will be displayed in between 9 to 22
        val min = randBetween(0, 59)
        val sec = randBetween(0, 59)
        val gc = GregorianCalendar(year, month, 1)
        val day = randBetween(1, gc.getActualMaximum(Calendar.DAY_OF_MONTH))
        gc.set(year, month, day, hour, min, sec)
        return dfDateTime.format(gc.time)
    }
}

fun String.getCustomDateString(format: String = "yyyy-MM-dd hh:mm:ss"): String {
    val date: Date
    val dateFormat = SimpleDateFormat(format, Locale.getDefault())
    date = if (this.isNotEmpty()) {
        dateFormat.parse(this) ?: Date()
    } else {
        dateFormat.parse(this) ?: Date()
    }
    val tmp = SimpleDateFormat("d", Locale.getDefault())
    var str = tmp.format(date)
    str += if (date.time in 11..13)
        "th"
    else {
        when {
            str.endsWith("1") -> "st"
            str.endsWith("2") -> "nd"
            str.endsWith("3") -> "rd"
            else -> "th"
        }
    }
    val tmpFullDate = SimpleDateFormat("MMMM yyyy", Locale.getDefault())
    val dateMY = tmpFullDate.format(date)
    return "$str $dateMY"
}

fun String.getExpiryDate(format: String = "MM:dd:yyyy"): String {
    val date: Date
    val dateFormat = SimpleDateFormat(format, Locale.getDefault())
    date = if (this.isNotEmpty()) {
        dateFormat.parse(this) ?: Date()
    } else {
        dateFormat.parse(this) ?: Date()
    }
    var cal = Calendar.getInstance()
    cal.time = date
    cal.add(Calendar.WEEK_OF_YEAR, 1)
//    cal.add(Calendar.WEEK_OF_MONTH, 1)
    val tmpFullDate = SimpleDateFormat("dd MMMM yyyy", Locale.getDefault())
    val dateMY = tmpFullDate.format(cal.time)
    return "$dateMY"
}