package com.namxu.multipicturecamera.base;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;

import androidx.appcompat.widget.AppCompatImageView;

import com.namxu.multipicturecamera.R;
import com.namxu.multipicturecamera.R.styleable;


public class RoundedImageView extends AppCompatImageView {
    private float cornerRadius;
    private boolean makeCircle;

    public RoundedImageView(Context context) {
        super(context);
    }

    public RoundedImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.RoundedImageView, 0, 0);
        this.cornerRadius = TypedValue.applyDimension(1, typedArray.getFloat(styleable.RoundedImageView_corner_radius, 0.0F), context.getResources().getDisplayMetrics());
        this.makeCircle = typedArray.getBoolean(styleable.RoundedImageView_make_circle, false);
        typedArray.recycle();
    }

    public RoundedImageView(Context context, AttributeSet attributeSet, int defStyle) {
        super(context, attributeSet, defStyle);
        TypedArray typedArray = context.obtainStyledAttributes(attributeSet, styleable.RoundedImageView, 0, 0);
        this.cornerRadius = TypedValue.applyDimension(1, typedArray.getFloat(styleable.RoundedImageView_corner_radius, 0.0F), context.getResources().getDisplayMetrics());
        this.makeCircle = typedArray.getBoolean(styleable.RoundedImageView_make_circle, false);
        typedArray.recycle();
    }

    protected void onDraw(Canvas canvas) {
        Drawable drawable = this.getDrawable();
        if (drawable != null) {
            if (this.getWidth() != 0 && this.getHeight() != 0) {
                Bitmap originalBitmap = ((BitmapDrawable) drawable).getBitmap();
                Bitmap bitmap = originalBitmap.copy(Config.ARGB_8888, true);
                int width = this.getWidth();
                int height = this.getHeight();
                Bitmap roundBitmap = this.getRoundedBitmap(bitmap, width, height);
                canvas.drawBitmap(roundBitmap, 0.0F, 0.0F, (Paint) null);
                roundBitmap.recycle();
            }
        }
    }

    public Bitmap getRoundedBitmap(Bitmap bitmap, int width, int height) {
        Bitmap earlyBitmap;
        if (bitmap.getWidth() == width && bitmap.getHeight() == width) {
            earlyBitmap = bitmap;
        } else {
            float smallest = (float) Math.min(bitmap.getWidth(), bitmap.getHeight());
            float factor = smallest / (float) width;
            earlyBitmap = Bitmap.createScaledBitmap(bitmap, (int) ((float) bitmap.getWidth() / factor), (int) ((float) bitmap.getHeight() / factor), false);
        }

        Bitmap outPutBitmap = Bitmap.createBitmap(width, height, Config.ARGB_8888);
        Canvas canvas = new Canvas(outPutBitmap);
        Paint paint = new Paint();
        Rect rect = new Rect(0, 0, width, height);
        RectF rectF = new RectF(0.0F, 0.0F, (float) width, (float) height);
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        paint.setDither(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(Color.parseColor("#FFFFFF"));
        if (this.makeCircle) {
            canvas.drawCircle((float) (width / 2) + 0.7F, (float) (height / 2) + 0.7F, (float) (width / 2) + 0.1F, paint);
        } else {
            canvas.drawRoundRect(rectF, this.cornerRadius, this.cornerRadius, paint);
        }

        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(earlyBitmap, rect, rectF, paint);
        return outPutBitmap;
    }
}
