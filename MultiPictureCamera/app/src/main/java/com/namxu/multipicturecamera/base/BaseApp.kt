package com.namxu.multipicturecamera.base

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.os.StrictMode
class BaseApp : Application() {

    override fun onCreate() {
        super.onCreate()
        photoApp = this
        context = applicationContext
        val builder: StrictMode.VmPolicy.Builder?
        builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())

    }

    companion object {
        @SuppressLint("StaticFieldLeak")
        lateinit var context: Context
            private set
        @SuppressLint("StaticFieldLeak")
        var photoApp: BaseApp? = null
            private set
        private val TAG = BaseApp::class.java.simpleName
    }
}

object Constants {

    val NOTIFICATION: String = "_open_notification"
    val VIDEO = "video"
    val IMAGE = "image"
    val BOTH = "both"
    val NONE = "none"
    val IMAGE_VIDEO_REQUEST_CODE = 1027
    val REQUEST_CODE_VIDEO = 9000
    val REQUEST_CODE_IMAGE = 9001
    val REQUEST_CODE_IMAGE_Edit = 9002
    val USER = "user"
    val COMPANION = "companion"
    val PLACES_KEY = "AIzaSyBfRJPn3O6lg2WY4iC1CvWA73S9dDAeW1g"
    val isFromStory = "isFromStory"
    val IS_NEED_TO_RETURN = "IS_NEED_TO_RETURN"
    const val DB_NAME = "Users"
    const val USERS_TABLE_NAME = "Users"
    const val BAMBUSER_APP_KEY =
        "noRXuPWn1wr8GGSqc4RJCg" //"a7X0aPDlrZGaDw8pmLEr2A"//QUQw4b0nqbwD7YTpejI5Ig
    const val BAMBUSER_API_KEY =
        "02xjqx8k9u5n9hq5n39dfv1c7" //"dwtbuzzvxjse7dfhwhh7843bh"//QUQw4b0nqbwD7YTpejI5Ig
    const val FACEBOOK_API_KEY = "653833748413596"
    const val stripe_publishKey = "pk_test_OBsspRSSppMi0WDwNCSCLvzU0057YIbQxi"
    const val stripe_secretKey = "sk_test_4mSBSnXJOP6aW5gF5NJZInsq009Myf1cgG"
    const val STRIPE_CONNECT_ACCOUNT =
        "https://connect.stripe.com/express/oauth/authorize?redirect_uri=https://www.duet.icu/stripe_redirect_uri&client_id=ca_FZWbcXWlfHtxCKUIKrL0nR7T6WTfcThu&scope=read_write&state=0O6NjOkks572wEYuwG5gvaR09EdJQd7X9HY8th1r"
    //const val BAMBUSER_APP_KEY = "hZAW9wgFU1dmUSV3jXm01A"
    //const val BAMBUSER_API_KEY = "df6a1szx403p4teewmfy8ski0"
    const val DELETE_MESG_CHAT = "Are you sure you want to delete this chat?"
    const val DELETE_CARD = "Are you sure you want to delete this card?"

    fun getUrlLink(lat: String, lng: String): String { //&zoom=10
        return "https://maps.googleapis.com/maps/api/staticmap?autoscale=2&zoom=4.02&size=1600x1400&maptype=roadmap&style=element:geometry%7Ccolor:0x212121&style=element:labels.text.fill%7Ccolor:0x757575&style=element:labels.icon%7Cvisibility:off&style=element:labels.text.stroke%7Ccolor:0x212121&style=feature:administrative%7Celement:geometry%7Ccolor:0x757575&style=feature:administrative.country%7Celement:labels.text.fill%7Ccolor:0x9e9e9e&style=feature:administrative.land_parcel%7Cvisibility:off&style=feature:administrative.locality%7Celement:labels.text.fill%7Ccolor:0xbdbdbd&style=feature:poi%7Celement:labels.text.fill%7Ccolor:0x757575&style=feature:poi.park%7Celement:geometry%7Ccolor:0x181818&style=feature:poi.park%7Celement:labels.text.fill%7Ccolor:0x616161&style=feature:poi.park%7Celement:labels.text.stroke%7Ccolor:0x1b1b1b&style=feature:road%7Celement:geometry.fill%7Ccolor:0x2c2c2c&style=feature:road%7Celement:labels.text.fill%7Ccolor:0x8a8a8a&style=feature:road.arterial%7Celement:geometry%7Ccolor:0x373737&style=feature:road.highway%7Celement:geometry%7Ccolor:0x3c3c3c&style=feature:road.highway.controlled_access%7Celement:geometry%7Ccolor:0x4e4e4e&style=feature:road.local%7Celement:labels.text.fill%7Ccolor:0x616161&style=feature:transit%7Celement:labels.text.fill%7Ccolor:0x757575&style=feature:water%7Ccolor:0x212121&style=feature:water%7Celement:geometry%7Ccolor:0x000000&style=feature:water%7Celement:geometry.fill%7Ccolor:0x3c4351&style=feature:water%7Celement:labels.text.fill%7Ccolor:0x3d3d3d&key=AIzaSyA-Uk5DNzYvHUS_8a9RbFsHE-99Aw3BwPM&format=gif&visual_refresh=true&markers=size:small%9Ccolor:0xFF2D55%7Clabel:1%7C$lat,$lng"
    }

    const val POSTS_PRIVACY = "postsPrivacy"
    const val STORIES_PRIVACY = "storiesPrivacy"

    const val AVAILABILITY = "availability"

}