package com.namxu.multipicturecamera.base

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.namxu.multipicturecamera.R
import com.namxu.multipicturecamera.base.helper.ItemTouchHelperAdapter
import com.namxu.multipicturecamera.base.helper.ItemTouchHelperViewHolder
import kotlinx.android.synthetic.main.photo_item.view.*
import java.io.File
import java.util.*

class ImagesAdapter(
    private var mData: ArrayList<PhotoAlbum>,
    private var mContext: BaseActivity,
    private var clickCallBack: (Int, ImageView) -> Unit
) :
    RecyclerView.Adapter<ImagesHolder>(), ItemTouchHelperAdapter {
    override fun onItemMove(fromPosition: Int, toPosition: Int): Boolean {
        Collections.swap(mData, fromPosition, toPosition)
        notifyItemMoved(fromPosition, toPosition)
        return true
    }

    override fun onItemDismiss(position: Int) {
        mData.removeAt(position)
        notifyItemRemoved(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImagesHolder {
        val view = LayoutInflater.from(mContext)
            .inflate(R.layout.photo_item, parent, false)
        return ImagesHolder(view)
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    override fun onBindViewHolder(holder: ImagesHolder, position: Int) {
        holder.bind(mData[position].filePath)
        holder.itemView.setOnClickListener {
            clickCallBack(position, holder.itemView.ivImage)
        }
    }


}

class ImagesHolder(itemView: View) : RecyclerView.ViewHolder(itemView), ItemTouchHelperViewHolder {
    override fun onItemSelected() {

    }

    override fun onItemClear() {

    }

    fun bind(uri: String) {
        with(itemView) {
            try {
                Glide.with(itemView)
                    .load(Uri.fromFile(File(uri)))
                    .placeholder(R.drawable.ic_image_placeholder)
                    .error(R.drawable.ic_image_placeholder)
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .into(ivImage)
            } catch (e: IllegalArgumentException) {
                e.printStackTrace()
            }
        }
    }

}