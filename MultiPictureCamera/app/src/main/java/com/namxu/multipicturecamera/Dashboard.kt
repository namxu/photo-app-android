package com.namxu.multipicturecamera

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.namxu.multipicturecamera.base.BaseActivity
import com.namxu.multipicturecamera.base.DataManager
import com.namxu.multipicturecamera.base.ImagesAdapter
import com.namxu.multipicturecamera.base.PhotoAlbum
import com.namxu.multipicturecamera.base.helper.GridItemDecoration
import com.namxu.multipicturecamera.base.helper.SimpleItemTouchHelperCallback
import com.stfalcon.imageviewer.StfalconImageViewer
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.overlay_view_image.view.*
import org.jetbrains.anko.alert
import java.io.File
import java.io.IOException

class Dashboard : BaseActivity() {
    var mData: ArrayList<PhotoAlbum> = ArrayList()
    var mAdapter: ImagesAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        btnAddPhoto.setOnClickListener {
            if (checkLocationPermission()) {
                dispatchTakePictureIntent()
//                val intent = Intent(this@Dashboard, MyCameraActivityNew::class.java)
//                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
//                intent.putExtra(Constants.IMAGE, Constants.IMAGE)
//                startActivity(intent)
            }
//            val intent = Intent(this@Dashboard, CameraView::class.java)

        }
        setupAdapter()
    }

    override fun onResume() {
        super.onResume()
        populateData()
    }

    private fun checkLocationPermission(): Boolean {
        return if (
            (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED)
            ||
            (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED)
            ||
            (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED)
            ||
            (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED) ||
            (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED)
            ||
            (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.RECORD_AUDIO
            ) != PackageManager.PERMISSION_GRANTED)
        ) {
            ActivityCompat.requestPermissions(
                this@Dashboard,
                arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                21212
            )
            false
        } else {
            true
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 21212) {
            if (grantResults.isNotEmpty() && grantResults.size >= 2) {
                var count = 0
                for (item in grantResults) {
                    if (item == PackageManager.PERMISSION_GRANTED) {
                        count++
                    }
                }
                if (count >= 6) {
                    dispatchTakePictureIntent()
//                    val intent = Intent(this@Dashboard, MyCameraActivityNew::class.java)
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
//                    intent.putExtra(Constants.IMAGE, Constants.IMAGE)
//                    startActivity(intent)
                } else {
                    alert(
                        "1. Please provide permission for locating geo tag on camera.\n2. Please provide permission for storage read and write.\n" +
                                "3. Please provide permission for camera.",
                        "Permission Needed"
                    ) {
                        positiveButton("Retry") {
                            checkLocationPermission()
                        }
                        negativeButton("Cancel") { finish() }
                        onCancelled { checkLocationPermission() }
                        isCancelable = false
                    }.show()
                }
            }
        }
    }

    private fun setupAdapter() {

        mAdapter = ImagesAdapter(mData, this@Dashboard) { positionItem, imageView ->
            val overLay = LayoutInflater.from(this).inflate(R.layout.overlay_view_image,null)
            val build = StfalconImageViewer.Builder<PhotoAlbum>(this@Dashboard, mData) { view, image ->
                Glide.with(view)
                    .load(Uri.fromFile(File(image.filePath)))
                    .placeholder(R.drawable.ic_image_placeholder)
                    .error(R.drawable.ic_image_placeholder)
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .into(view)
            }
                .allowZooming(true)
                .withOverlayView(overLay)
                .withBackgroundColor(getColor(android.R.color.black))
                .withHiddenStatusBar(true)
                .withTransitionFrom(imageView)
                .allowSwipeToDismiss(true)
                .withStartPosition(positionItem)
                .build()
            overLay.ivCross.setOnClickListener {
                build.dismiss()
            }


            build.show(true)
        }
        rvList.adapter = mAdapter
        rvList.layoutManager = GridLayoutManager(this@Dashboard, 3, RecyclerView.VERTICAL, false)
        rvList.addItemDecoration(GridItemDecoration(2, 3))
        (rvList.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        val callback = SimpleItemTouchHelperCallback(mAdapter)
        val mItemTouchHelper = ItemTouchHelper(callback)
        mItemTouchHelper.attachToRecyclerView(rvList)
    }

    private fun populateData() {
        mData.clear()
        mData.addAll(DataManager.getDashboardPhotoAlbum())
        mAdapter?.notifyDataSetChanged()
    }

    val REQUEST_TAKE_PHOTO = 1
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
            val list = DataManager.getDashboardPhotoAlbum()
            list.add(0, PhotoAlbum(outerFile?.absolutePath ?: ""))
            DataManager.saveDashboardPhotoAlbum(list)
//            populateData()
            // If you are using Glide.
        }
    }
    private var outerFile:File? = null
    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createFileReturn("NewFile")
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                    null
                }
                outerFile = photoFile
                // Continue only if the File was successfully created
                photoFile?.also {
                    val photoURI: Uri = Uri.fromFile(photoFile)
                        //FileProvider.getUriForFile(
                    //                        this,
                    //                        "com.namxu.multipicturecamera.fileprovider",
                    //                        it
                        //                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)

                    startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO)
                }
            }
        }
    }
}
