@file:Suppress("unused")

package com.namxu.multipicturecamera.base.prefrencecustom

import com.google.gson.Gson

import java.lang.reflect.Type

//import_image com.google.gson.Gson;

class GsonConverter : JsonConverter {

    private val gson: Gson

    constructor(gson: Gson) {
        this.gson = gson
    }

    constructor() {
        gson = Gson()
    }

    override fun <T> fromJson(json: String, typeOfT: Type): T {
        return gson.fromJson(json, typeOfT)
    }

    override fun <T> toJson(`object`: T, typeOfT: Type): String {
        return gson.toJson(`object`, typeOfT)
    }
}
