package com.namxu.multipicturecamera.base.prefrencecustom

import android.content.SharedPreferences
import java.util.*

internal class PreferencesAccessorsProvider(private val preferences: SharedPreferences, private val editor: SharedPreferences.Editor) : AccessorsProvider {
    private val accessors = HashMap<Class<*>, Accessor<*>>()

    init {
        Preconditions.checkNotNull(preferences, "preferences == null")
        Preconditions.checkNotNull(editor, "editor == null")
        createAccessors()
    }

    override fun getAccessors(): Map<Class<*>, Accessor<*>> {
        return accessors
    }

    private fun createAccessors() {
        createBooleanAccessor()
        createFloatAccessor()
        createIntegerAccessor()
        createLongAccessor()
        createDoubleAccessor()
        createStringAccessor()
    }

    private fun createBooleanAccessor() {
        accessors[Boolean::class.java] = object : Accessor<Boolean> {
            override fun get(key: String, defaultValue: Boolean?): Boolean? {
                return preferences.getBoolean(key, defaultValue ?: false)
            }

            override fun put(key: String, value: Boolean?) {
                editor.putBoolean(key, value ?: false).apply()
            }
        }
    }

    private fun createFloatAccessor() {
        accessors[Float::class.java] = object : Accessor<Float> {
            override fun get(key: String, defaultValue: Float?): Float? {
                return preferences.getFloat(key, defaultValue ?: 0.0F)
            }

            override fun put(key: String, value: Float?) {
                editor.putFloat(key, value ?: 0.0F).apply()
            }
        }
    }

    private fun createIntegerAccessor() {
        accessors[Int::class.java] = object : Accessor<Int> {
            override fun get(key: String, defaultValue: Int?): Int? {
                return preferences.getInt(key, defaultValue ?: 0)
            }

            override fun put(key: String, value: Int?) {
                editor.putInt(key, value ?: 0).apply()
            }
        }
    }

    private fun createLongAccessor() {
        accessors[Long::class.java] = object : Accessor<Long> {
            override fun get(key: String, defaultValue: Long?): Long? {
                return preferences.getLong(key, defaultValue ?: 0)
            }

            override fun put(key: String, value: Long?) {
                editor.putLong(key, value ?: 0).apply()
            }
        }
    }

    private fun createDoubleAccessor() {
        accessors[Double::class.java] = object : Accessor<Double> {
            override fun get(key: String, defaultValue: Double?): Double? {
                return java.lang.Double.valueOf(preferences.getString(key, defaultValue.toString())
                        ?: "0.0")
            }

            override fun put(key: String, value: Double?) {
                editor.putString(key, value.toString()).apply()
            }
        }
    }

    private fun createStringAccessor() {
        accessors[String::class.java] = object : Accessor<String> {
            override fun get(key: String, defaultValue: String): String {
                return preferences.getString(key, defaultValue) ?: ""
            }

            override fun put(key: String, value: String) {
                editor.putString(key, value).apply()
            }
        }
    }
}
