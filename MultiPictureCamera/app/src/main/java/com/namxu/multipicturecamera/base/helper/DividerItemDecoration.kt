@file:Suppress("unused", "ControlFlowWithEmptyBody", "ConvertSecondaryConstructorToPrimary")

package com.namxu.multipicturecamera.base.helper

import android.content.Context
import android.graphics.Canvas
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.util.TypedValue
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlin.math.roundToInt


class DividerItemDecoration : RecyclerView.ItemDecoration {
    private val mATTRS = intArrayOf(android.R.attr.listDivider)
    private var mHorizontalList = LinearLayoutManager.HORIZONTAL

    private var mVerticalList = LinearLayoutManager.VERTICAL

    private var mDivider: Drawable? = null
    private var mOrientation: Int = 0
    private var context: Context? = null
    private var margin: Int = 0

    constructor(context: Context, orientation: Int, margin: Int) {
        this.context = context
        this.margin = margin
        val a = context.obtainStyledAttributes(mATTRS)
        mDivider = a.getDrawable(0)
        a.recycle()
        setOrientation(orientation)
    }

    private fun setOrientation(orientation: Int) {
        require(!(orientation != mHorizontalList && orientation != mVerticalList)) { "invalid orientation" }
        mOrientation = orientation
    }

    override
    fun onDrawOver(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        if (mOrientation == mVerticalList) {
            //   drawVertical(c, parent)
        } else {
            //  drawHorizontal(c, parent)
        }
    }

    fun drawVertical(c: Canvas, parent: RecyclerView) {
        val left = parent.paddingLeft
        val right = parent.width - parent.paddingRight

        val childCount = parent.childCount
        for (i in 0 until childCount) {
            val child = parent.getChildAt(i)
            val params = child
                    .layoutParams as RecyclerView.LayoutParams
            val top = child.bottom + params.bottomMargin
            val bottom = top + (mDivider?.intrinsicHeight ?: 0)
            mDivider?.setBounds(left + dpToPx(margin), top, right - dpToPx(margin), bottom)
            mDivider?.draw(c)
        }
    }

    fun drawHorizontal(c: Canvas, parent: RecyclerView) {
        val top = parent.paddingTop
        val bottom = parent.height - parent.paddingBottom

        val childCount = parent.childCount
        for (i in 0 until childCount) {
            val child = parent.getChildAt(i)
            val params = child
                    .layoutParams as RecyclerView.LayoutParams
            val left = child.right + params.rightMargin
            val right = left + (mDivider?.intrinsicHeight ?: 0)
            mDivider?.setBounds(left, top + dpToPx(margin), right, bottom - dpToPx(margin))
            mDivider?.draw(c)
        }
    }

    override
    fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        if (mOrientation == mVerticalList) {
            outRect.set(0, 0, 0, mDivider?.intrinsicHeight ?: 0)
        } else {
            outRect.set(margin, 0, margin, 0)
        }
    }

    private fun dpToPx(dp: Int): Int {
        val r = context!!.resources
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), r.displayMetrics).roundToInt()
    }

}