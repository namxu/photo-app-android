@file:Suppress("UNCHECKED_CAST", "unused", "DEPRECATION")

package com.namxu.multipicturecamera.base.prefrencecustom

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.core.util.Preconditions
import io.reactivex.Observable

class PhotoMultiPreference
@SuppressLint("CommitPrefEdits")
constructor(
        /**
         * Returns SharedPreferences in case, we want to manipulate them without Prefser.
         *
         * @return SharedPreferences instance of SharedPreferences
         */
        private val preferences: SharedPreferences,
        private val jsonConverter: JsonConverter
) {
    private val editor: SharedPreferences.Editor
    private val accessorProvider: AccessorsProvider

    /**
     * Creates Prefser object with default SharedPreferences from PreferenceManager.
     * with JsonConverter implementation
     *
     * @param context       Android Context
     * @param jsonConverter Json Converter
     */
    @JvmOverloads
    constructor(context: Context, jsonConverter: JsonConverter = GsonConverter()) : this(PreferenceManager.getDefaultSharedPreferences(context), jsonConverter)

    /**
     * Creates Prefser object with provided object of SharedPreferences,
     * which will be wrapped.
     *
     * @param sharedPreferences instance of SharedPreferences
     */
    constructor(sharedPreferences: SharedPreferences) : this(sharedPreferences, GsonConverter())

    init {
        Preconditions.checkNotNull(preferences, "sharedPreferences == null")
        Preconditions.checkNotNull(jsonConverter, "jsonConverter == null")
        this.editor = preferences.edit()
        this.accessorProvider = PreferencesAccessorsProvider(preferences, editor)
    }

    /**
     * Checks if preferences contains value with a given key
     *
     * @param key provided key
     * @return true if preferences contains key and false if not
     */
    operator fun contains(key: String): Boolean {
        return preferences.contains(key)
    }

    /**
     * Gets value from SharedPreferences with a given key and type
     * as a RxJava Observable, which can be subscribed.
     * If value is not found, we can return defaultValue.
     * Emit preference as first element of the stream even if preferences wasn't changed.
     *
     * @param key          key of the preference
     * @param classOfT     class of T (e.g. String.class)
     * @param defaultValue default value of the preference (e.g. "" or "undefined")
     * @param <T>          return type of the preference (e.g. String)
     * @return Observable value from SharedPreferences associated with given key or default value
    </T> */
    fun <T> getAndObserve(key: String, classOfT: Class<T>, defaultValue: T): Observable<T> {
        return getAndObserve(key, TypeToken.fromClass(classOfT), defaultValue)
    }

    /**
     * Gets value from SharedPreferences with a given key and type token
     * as a RxJava Observable, which can be subscribed
     * If value is not found, we can return defaultValue.
     * Emit preference as first element of the stream even if preferences wasn't changed.
     *
     * @param key          key of the preference
     * @param typeTokenOfT type token of T (e.g. `new TypeToken<List<String>> {})
     * defaultValue default value of the preference (e.g. "" or "undefined")
     * <T>          return type of the preference (e.g. String)
     * Observable value from SharedPreferences associated with given key or default value`
     */
    private fun <T> getAndObserve(key: String, typeTokenOfT: TypeToken<T>,
                                  defaultValue: T): Observable<T> {
        return observe(key, typeTokenOfT, defaultValue) // start observing
                .mergeWith(
                        Observable.defer { Observable.just(get(key, typeTokenOfT, defaultValue)) })
    }

    /**
     * Gets value from SharedPreferences with a given key and type
     * as a RxJava Observable, which can be subscribed.
     * If value is not found, we can return defaultValue.
     *
     * @param key          key of the preference
     * @param classOfT     class of T (e.g. String.class)
     * @param defaultValue default value of the preference (e.g. "" or "undefined")
     * @param <T>          return type of the preference (e.g. String)
     * @return Observable value from SharedPreferences associated with given key or default value
    </T> */
    fun <T> observe(key: String, classOfT: Class<T>,
                    defaultValue: T): Observable<T> {
        Preconditions.checkNotNull(key, KEY_IS_NULL)
        Preconditions.checkNotNull(classOfT, CLASS_OF_T_IS_NULL)

        return observe(key, TypeToken.fromClass(classOfT), defaultValue)
    }

    /**
     * Gets value from SharedPreferences with a given key and type token
     * as a RxJava Observable, which can be subscribed.
     * If value is not found, we can return defaultValue.
     *
     * @param key          key of the preference
     * @param typeTokenOfT type token of T (e.g. `new TypeToken<List<String>> {})
     * defaultValue default value of the preference (e.g. "" or "undefined")
     * <T>          return type of the preference (e.g. String)
     * Observable value from SharedPreferences associated with given key or default value`
     */
    private fun <T> observe(key: String,
                            typeTokenOfT: TypeToken<T>, defaultValue: T): Observable<T> {
        Preconditions.checkNotNull(key, KEY_IS_NULL)
        Preconditions.checkNotNull(typeTokenOfT, TYPE_TOKEN_OF_T_IS_NULL)

        return observePreferences().filter { filteredKey -> key == filteredKey }.map { get(key, typeTokenOfT, defaultValue) }
    }

    /**
     * Gets value from SharedPreferences with a given key and type.
     * If value is not found, we can return defaultValue.
     *
     * @param key          key of the preference
     * @param classOfT     class of T (e.g. `String.class`)
     * @param defaultValue default value of the preference (e.g. "" or "undefined")
     * @param <T>          return type of the preference (e.g. String)
     * @return value from SharedPreferences associated with given key or default value
    </T> */
    operator fun <T> get(key: String, classOfT: Class<T>, defaultValue: T): T? {
        Preconditions.checkNotNull(key, KEY_IS_NULL)
        Preconditions.checkNotNull(classOfT, CLASS_OF_T_IS_NULL)

        return if (!contains(key) && defaultValue == null) {
            null
        } else get(key, TypeToken.fromClass(classOfT), defaultValue)

    }

    /**
     * Gets value from SharedPreferences with a given key and type.
     * If value is not found, we can return defaultValue.
     *
     * @param key          key of the preference
     * @param typeTokenOfT type token of T (e.g. `new TypeToken<List<String>> {})
     * defaultValue default value of the preference (e.g. "" or "undefined")
     * <T>          return type of the preference (e.g. String)
     * value from SharedPreferences associated with given key or default value`
     */
    operator fun <T> get(key: String, typeTokenOfT: TypeToken<T>, defaultValue: T): T {
        Preconditions.checkNotNull(key, KEY_IS_NULL)
        Preconditions.checkNotNull(typeTokenOfT, TYPE_TOKEN_OF_T_IS_NULL)

        val typeOfT = typeTokenOfT.type

        for ((key1, value) in accessorProvider.accessors) {
            if (typeOfT == key1) {
                val accessor = value as Accessor<T>
                return accessor.get(key, defaultValue)
            }
        }

        return if (contains(key)) {
            jsonConverter.fromJson(preferences.getString(key, null) ?: "", typeOfT)
        } else {
            defaultValue
        }
    }

    /**
     * returns RxJava Observable from SharedPreferences used inside Prefser object.
     * You can subscribe this Observable and every time,
     * when SharedPreferences will change, subscriber will be notified
     * about that and you will be able to read
     * key of the value, which has been changed.
     *
     * @return Observable with String containing key of the value in default SharedPreferences
     */
    private fun observePreferences(): Observable<String> {
        return Observable.create { e ->
            preferences.registerOnSharedPreferenceChangeListener { _, key ->
                if (!e.isDisposed) {
                    e.onNext(key)
                }
            }
        }
    }

    /**
     * Puts value to the SharedPreferences.
     *
     * @param key   key under which value will be stored
     * @param value value to be stored
     */
    fun <T : Any> put(key: String, value: T) {
        Preconditions.checkNotNull(value, VALUE_IS_NULL)
        put(key, value, TypeToken.fromValue(value))
    }

    /**
     * Puts value to the SharedPreferences.
     *
     * @param key          key under which value will be stored
     * @param value        value to be stored
     * @param typeTokenOfT type token of T (e.g. `new TypeToken<> {})`
     */
    fun <T : Any> put(key: String, value: T, typeTokenOfT: TypeToken<T>) {
        Preconditions.checkNotNull(key, KEY_IS_NULL)
        Preconditions.checkNotNull(value, VALUE_IS_NULL)
        Preconditions.checkNotNull(typeTokenOfT, TYPE_TOKEN_OF_T_IS_NULL)

        if (!accessorProvider.accessors.containsKey(value.javaClass)) {
            val jsonValue = jsonConverter.toJson(value, typeTokenOfT.type)
            editor.putString(key, jsonValue).apply()
            return
        }

        val classOfValue = value.javaClass

        for ((key1, value1) in accessorProvider.accessors) {
            if (classOfValue == key1) {
                val accessor = value1 as Accessor<T>
                accessor.put(key, value)
            }
        }
    }

    /**
     * Removes value defined by a given key.
     *
     * @param key key of the preference to be removed
     */
    fun remove(key: String) {
        Preconditions.checkNotNull(key, KEY_IS_NULL)
        if (!contains(key)) {
            return
        }

        editor.remove(key).apply()
    }

    /**
     * Clears all SharedPreferences.
     */
    fun clear() {
        if (size() == 0) {
            return
        }

        editor.clear().apply()
    }

    /**
     * Returns number of all items stored in SharedPreferences.
     *
     * @return number of all stored items
     */
    fun size(): Int {
        return preferences.all.size
    }

    companion object {
        private const val KEY_IS_NULL = "key == null"
        private const val CLASS_OF_T_IS_NULL = "classOfT == null"
        private const val TYPE_TOKEN_OF_T_IS_NULL = "typeTokenOfT == null"
        private const val VALUE_IS_NULL = "value == null"
    }
}